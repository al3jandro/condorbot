import re
from bot import wppbot

bot = wppbot('robozin')
# bot.treina('treino')
# bot.inicia('+55 41 9881-9501')
bot.inicia('Xiore')
bot.saudacao(['Bot: Oi, sou o robozin de Condor!','Bot: Use # no início para falar comigo'])
# bot.image()
ultimo_texto = ''

while True:

    texto = bot.escuta()

    if texto != ultimo_texto and re.match(r'^#', texto):

        ultimo_texto = texto
        texto = texto.replace('#', '')
        texto = texto.lower()

        if (texto == 'aprender' or texto == ' aprender' or texto == 'ensinar' or texto == ' ensinar'):
            bot.aprender(texto,'bot: Escreva a pergunta e após o ? a resposta.','bot: Obrigado por ensinar! Agora já sei!','bot: Você escreveu algo errado! Comece novamente..')
        elif (texto == 'noticias' or texto == ' noticias' or texto == 'noticia' or texto == ' noticia' or texto == 'notícias' or texto == ' notícias' or texto == 'notícia' or texto == ' notícia'):
            bot.noticias()
        elif (texto == 'receitas' or texto == ' receitas' or texto == 'receita' or texto == ' receita'):
            bot.receitas()
        else:
            bot.responde(texto)